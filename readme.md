# To Do:

I need to implement a turn-type of class (possibly a singleton) that will give
each monster and the user a turn and handle dishing out of the VitalPackages.

I need to make sure that there is a way to stop everything and let the user knowthat they have died.

I probably want to add an "execute attack" button for when the user is done deciding on weapon and power up.

I need to use the strategy and composite patterns.  Possibly use stategy with attacks (as each attack will be different) and also composite with attack.

Still need to level up user.  I think I will do this by adding 5 point on each level to attack strength.

Enemies need to select a random weapon and power up.

## GUI:
![Gui][1]

[1]: gui.png "GUI"
