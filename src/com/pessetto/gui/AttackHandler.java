package com.pessetto.gui;

import com.pessetto.invokers.TurnInvoker;
import com.pessetto.monsters.MonsterFactory;
import com.pessetto.user.User;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AttackHandler implements EventHandler<ActionEvent> {
	private MainPane pane;
	
	public AttackHandler(MainPane pane)
	{
		this.pane = pane;
	}

	@Override
	public void handle(ActionEvent arg0) {
		TurnInvoker turns = TurnInvoker.getInstance();
		turns.addAttack(MonsterFactory.getInstance().getSelectedMonsterIndex(),
				User.getInstance().getWeapon());
		
		addMonsterAttacks();
		
		turns.executeAttacks();
		
		pane.reloadEnemiesPane();
	}
	
	public void addMonsterAttacks()
	{
		for(int i = 0; i < 4; ++i)
		{
			TurnInvoker.getInstance().addAttack(User.getInstance(), MonsterFactory.getInstance().getMonster(i).getWeapon());
		}
	}

}
