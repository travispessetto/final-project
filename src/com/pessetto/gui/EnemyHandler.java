package com.pessetto.gui;
import com.pessetto.io.ConsoleWriter;
import com.pessetto.monsters.MonsterFactory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class EnemyHandler implements EventHandler<ActionEvent> {

	private MainPane pane;
	private String name;
	private int index;
	public EnemyHandler(MainPane pane, String name, int index)
	{
		this.pane = pane;
		this.name = name;
		this.index = index;
	}
	@Override
	public void handle(ActionEvent event) {
		pane.updateEnemy(name, index);
		MonsterFactory.getInstance().setSelectedMonsterIndex(index);
		ConsoleWriter.getInstance().writeLine("Enemy "+String.valueOf(index + 1) + " selected.");	
	}

}
