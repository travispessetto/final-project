package com.pessetto.gui;


import com.pessetto.io.ConsoleWriter;
import com.pessetto.monsters.Monster;
import com.pessetto.monsters.MonsterFactory;
import com.pessetto.user.User;
import com.pessetto.weapons.Arrow;
import com.pessetto.weapons.Axe;
import com.pessetto.weapons.None;
import com.pessetto.weapons.Sword;
import com.pessetto.weapons.powerups.Fire;
import com.pessetto.weapons.powerups.Magic;
import com.pessetto.weapons.powerups.Poison;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.FlowPane;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputControl;

@SuppressWarnings("restriction")
public class MainPane extends Application
{
	private Label enemySelected;
	private BorderPane root;
	private Scene scene;
	private GridPane enemyPane;
	private TextArea console;
	private BorderPane control;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		ConsoleWriter.setupAndGetInstance(this);
		MonsterFactory.getInstance().setMainPaneReference(this);;
		
		root = new BorderPane();
		
		scene = new Scene(root,720,360);
		scene.setFill(Color.LIGHTGRAY);
		
		enemySelected = new Label("None");
		
		root.setLeft(adPane());
		root.setCenter(controlPane());
		
		primaryStage.setTitle("D&D");
		primaryStage.setScene(scene);
		
		primaryStage.show();
		
	}
	
	public void writeToConsole(String line)
	{
		if(console == null)
		{
			console = new TextArea();
			console.setWrapText(true);
		}
		console.appendText(line+"\n\r");
		console.positionCaret(console.getText().length());
	}
	
	private FlowPane adPane()
	{
		FlowPane adPane = new FlowPane();
		Image adImg = new Image("http://i.imgur.com/EaIlbCW.png");
		ImageView adView = new ImageView(adImg);
		adPane.getChildren().add(adView);
		adPane.setStyle("-fx-background-color: #ffffff;");
		return adPane;
	}
	
	private BorderPane controlPane()
	{
		BorderPane control = new BorderPane();
		control.setTop(enemiesPane());
		control.setCenter(console);
		control.setBottom(userPane());
		this.control = control;
		return control;
	}
	
	public void reloadEnemiesPane()
	{
		control.setTop(enemiesPane());
		root.setCenter(controlPane());
	}
	private GridPane enemiesPane()
	{
		enemyPane = new GridPane();
		Button enemyBtn[] = new Button[4];
		
		enemyBtn[0] = new Button("Enemy 1");
		enemyBtn[1] = new Button("Enemy 2");
		enemyBtn[2] = new Button("Enemy 3");
		enemyBtn[3] = new Button("Enemy 4");
		
		enemyBtn[0].setOnAction(new EnemyHandler(this,"Enemy 1",0));
		enemyBtn[1].setOnAction(new EnemyHandler(this,"Enemy 2",1));
		enemyBtn[2].setOnAction(new EnemyHandler(this,"Enemy 3",2));
		enemyBtn[3].setOnAction(new EnemyHandler(this,"Enemy 4",3));

		
		enemyPane.addRow(1,enemyBtn);
		
		Label selectedTitle = new Label("Selected:");
		enemyPane.add(selectedTitle,1,2);
		enemyPane.add(enemySelected, 2, 2);
		
		if(MonsterFactory.getInstance().getSelectedMonster() != null)
		{
			Label health[] = {new Label("Health:"), new Label(String.valueOf(MonsterFactory.getInstance().getSelectedMonster().getVitals().life))};
			Label stamina[] = {new Label("Stamina:"), new Label(String.valueOf(MonsterFactory.getInstance().getSelectedMonster().getVitals().stamina))};
			enemyPane.addRow(3, health);
			enemyPane.addRow(4, stamina);
		}
		
		
		return enemyPane;
	}
	
	
	public void updateEnemy(String name, int index)
	{
		enemySelected = new Label(name);
		MonsterFactory.getInstance().setSelectedMonsterIndex(index);
		root.setCenter(controlPane());
	}
	
	public GridPane userPane()
	{
		Button weapons[] = {new Button("None"), new Button("Axe"), new Button("Sword"),
		 new Button("Bow & Arrow")};
		
		weapons[0].setOnAction(new WeaponHandler(new None(),"None"));
		weapons[1].setOnAction(new WeaponHandler(new Axe(), "Axe"));
		weapons[2].setOnAction(new WeaponHandler(new Sword(),"Sword"));
		weapons[3].setOnAction(new WeaponHandler(new Arrow(), "Bow and Arrow"));
		
		Button powerUps[] = {new Button("Fire"), new Button("Magic"), new Button("Poison")};
		powerUps[0].setOnAction(new PowerUpHandler(new Fire(), "Fire"));
		powerUps[1].setOnAction(new PowerUpHandler(new Magic(), "Magic"));
		powerUps[2].setOnAction(new PowerUpHandler(new Poison(),"Poison"));
		
		Button attack = new Button("Run attack turns");
		attack.setOnAction(new AttackHandler(this));
		
		Label health[] = {new Label("Health:"), new Label(String.valueOf(User.getInstance().getVitals().life))};
		Label stamina[] = {new Label("Stamina:"), new Label(String.valueOf(User.getInstance().getVitals().stamina))};
		
		
		GridPane userPanel = new GridPane();
		userPanel.addRow(1, weapons);
		userPanel.addRow(2, powerUps);
		
		userPanel.addRow(3, health);
		userPanel.addRow(4, stamina);
		userPanel.add(attack, 3, 2);
		
		
		return userPanel;
	}
	
	
	public static void main(String[] args)
	{
		launch(args);
	}

}
