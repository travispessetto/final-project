package com.pessetto.gui;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.user.User;
import com.pessetto.weapons.powerups.PowerUp;

import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class PowerUpHandler implements EventHandler<ActionEvent>{
	private String name;
	private PowerUp powerUp;
	
	public PowerUpHandler(PowerUp powerUp, String name)
	{
		this.name = name;
		this.powerUp = powerUp;
	}
	@Override
	public void handle(ActionEvent arg0) {
		if(User.getInstance().addPowerUp(this.powerUp) == true)
		{
			ConsoleWriter.getInstance().writeLine(name + " power up added to weapon's power up chain.");
		}
		else
		{
			ConsoleWriter.getInstance().writeLine("You have not killed enough enemies to add this power up"+
					"to your power up chain.");
		}
	}

}
