package com.pessetto.gui;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.user.User;
import com.pessetto.weapons.Weapon;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class WeaponHandler implements EventHandler<ActionEvent>  {

	private Weapon weapon;
	private String name;
	public WeaponHandler(Weapon weapon, String name)
	{
		this.weapon = weapon;
		this.name = name;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		User.getInstance().setWeapon(weapon);
		ConsoleWriter.getInstance().writeLine("You selected "+name+ " as your weapon.");
	}

}
