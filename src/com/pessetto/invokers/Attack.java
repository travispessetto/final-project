package com.pessetto.invokers;

import com.pessetto.monsters.MonsterFactory;
import com.pessetto.vitals.Liveable;
import com.pessetto.weapons.Weapon;

public class Attack {
	private Liveable target;
	private int monsterIndex;
	private Weapon weapon;
	
	public Attack(Liveable target, Weapon weapon)
	{
		this.target = target;
		this.weapon = weapon;
		this.monsterIndex = -1;
	}
	
	public Attack(int monsterIndex, Weapon weapon)
	{
		this.monsterIndex = monsterIndex;
		this.weapon = weapon;
	}
	
	public void execute()
	{
		if(target != null)
		{	
			target.takeVitals(weapon.getDamage());
		}
		else if(monsterIndex >= 0)
		{
			MonsterFactory.getInstance().attackMonster(monsterIndex, weapon.getDamage());
		}
		
	}
}
