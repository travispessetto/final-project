package com.pessetto.invokers;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.user.User;
import com.pessetto.vitals.Liveable;
import com.pessetto.weapons.Weapon;

public class TurnInvoker {
	
	private static TurnInvoker instance;
	private Queue<Attack> queue;
	private TurnInvoker()
	{
		queue = new LinkedBlockingQueue<Attack>();
	}
	
	public static TurnInvoker getInstance()
	{
		if(instance == null) instance = new TurnInvoker();
		return instance;
	}
	
	public void addAttack(Liveable target, Weapon weapon)
	{
		queue.add(new Attack(target, weapon));
	}
	
	public void addAttack(int monsterIndex, Weapon weapon)
	{
		queue.add(new Attack(monsterIndex,weapon));
	}
	
	public void executeAttacks()
	{
		if(User.getInstance().isDead())
		{
			ConsoleWriter.getInstance().writeLine("You are dead.  Restart program to play again.");
		}
		else
		{
			while(queue.isEmpty() == false)
			{
				queue.remove().execute();
			}
		}
	}

}
