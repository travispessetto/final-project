package com.pessetto.io;

import com.pessetto.gui.MainPane;
import java.io.*;

public class ConsoleWriter {
	private static ConsoleWriter instance;
	private MainPane pane;
	
	private ConsoleWriter(){}
	
	public static ConsoleWriter setupAndGetInstance(MainPane pane)
	{
		if(instance == null)
		{
			instance = new ConsoleWriter();
			instance.pane = pane;
		}
		return instance;
	}
	
	public static ConsoleWriter getInstance()
	{
		if(instance == null)
		{
			System.out.println("Must init in MainPane where console is in start method.");
		}
		return instance;
	}
	
	public void writeLine(String line)
	{
		pane.writeToConsole(line);
	}
}
