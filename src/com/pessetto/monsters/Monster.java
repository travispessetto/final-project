package com.pessetto.monsters;

import java.util.Random;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.vitals.Liveable;
import com.pessetto.weapons.Arrow;
import com.pessetto.weapons.Axe;
import com.pessetto.weapons.None;
import com.pessetto.weapons.Sword;
import com.pessetto.weapons.Weapon;

public class Monster extends Liveable{
	public Monster(String name, int life, int stamina)
	{
		super(name);
		vitals.life = life;
		vitals.stamina = stamina;
		Random randomGen = new Random();
		int choosenWeapon = randomGen.nextInt(3) + 0;
		switch(choosenWeapon)
		{
		case 0:
			weapon = new None();
			break;
		case 1:
			weapon = new Axe();
			break;
		case 2:
			weapon = new Sword();
			break;
		case 3: 
			weapon = new Arrow();
			break;
		default:
			ConsoleWriter.getInstance().writeLine("Random Weapon for monster fell through.  Using Sword");
			weapon = new Sword();
		}
		ConsoleWriter.getInstance().writeLine("A new monster was just created with "+ String.valueOf(life)+
				" life and " + String.valueOf(stamina) + " stamina.");
	}
	
	/*public Monster(String name)
	{
		this(name,100,100);
	}*/
}
