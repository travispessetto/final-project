package com.pessetto.monsters;

import java.util.Random;

import com.pessetto.gui.MainPane;
import com.pessetto.io.ConsoleWriter;
import com.pessetto.vitals.VitalPackage;

/**************************
 * 
 * @author Travis Pessetto
 * Descrion:  This class is responsible for generating mosters
 * as well as keeping track of them. It can hold 4 monsters
 * at a time.
 ***************************/
public class MonsterFactory {
	private static MonsterFactory instance;
	private Level userLevel = Level.EASY;
	private Monster monsters[] = new Monster[4];
	private int deadMonsterCount = 0;
	private int selectedMonster;
	private MainPane mainPane;
	
	public enum Level {EASY, AVERAGE, HARD};
	private MonsterFactory()
	{
		for(int i = 0; i < 4; ++i)
		{
			monsters[i] = newMonster(i);
		}
		selectedMonster = 0;
	}
	
	public void setMainPaneReference(MainPane pane)
	{
		this.mainPane = pane;
	}
	
	private void updateEnemiesPane()
	{
		if(mainPane != null) mainPane.updateEnemy("Enemy "+ (getSelectedMonsterIndex() + 1), getSelectedMonsterIndex());
	}
	
	private Monster newMonster(int index)
	{
		Monster monster = null;
		Random randomGen = new Random();
		switch(userLevel)
		{
		case EASY:
				monster =  new Monster("Enemy " + (index + 1),randomGen.nextInt(50) + 25,50);
				break;
		case AVERAGE:
				monster = new Monster("Enemy" + (index + 1), randomGen.nextInt(150) + 75, randomGen.nextInt(150) + 75);
				break;
		case HARD:
				monster = new Monster("Enemy" + (index + 1), randomGen.nextInt(300) + 100, randomGen.nextInt(300) + 100);
				break;
		}
		return monster;
	}
	
	public static MonsterFactory getInstance()
	{
		if(instance == null)
		{
			instance = new MonsterFactory();
		}
		return instance;
	}
	
	public void setLevel(Level level)
	{
		userLevel = level;
	}
	
	public void attackMonster(int index, VitalPackage vitals)
	{
		monsters[index].takeVitals(vitals);
		if(monsters[index].isDead())
		{
			++deadMonsterCount;
			monsters[index] = newMonster(index);
			ConsoleWriter.getInstance().writeLine("You have killed " + deadMonsterCount +" monsters.");
			updateEnemiesPane();
		}
	}
	
	public Monster getMonster(int index)
	{
		return monsters[index];
	}
	
	public int getDeadMonsterCount()
	{
		return deadMonsterCount;
	}
	
	public void setSelectedMonsterIndex(int index)
	{
		selectedMonster = index;
	}
	
	public int getSelectedMonsterIndex()
	{
		return selectedMonster;
	}
	
	public Monster getSelectedMonster()
	{
		return monsters[selectedMonster];
	}
	
	
	
	
}
