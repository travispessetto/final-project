package com.pessetto.user;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.monsters.MonsterFactory;
import com.pessetto.vitals.Liveable;
import com.pessetto.weapons.Weapon;
import com.pessetto.weapons.None;
import com.pessetto.weapons.powerups.PowerUp;

public class User extends Liveable {
	private static User instance;
	
	private User()
	{
		super("user");
		weapon = new None();
		vitals.life = 10000;
		vitals.stamina = 20000;
	}
	
	public static User getInstance()
	{
		if(instance == null)
		{
			instance = new User();
		}
		return instance;
	}
	
	
	public boolean addPowerUp(PowerUp powerUp)
	{
		//strength is the same as dead monster count.
		int strength = MonsterFactory.getInstance().getDeadMonsterCount();
		if(strength >= weapon.getPowerChainRequiredKills() + powerUp.getRequiredKillsForSelfOnly())
		{
			weapon.addPowerUp(powerUp);
			return true;
		}
		else
		{
			ConsoleWriter.getInstance().writeLine("To chain this power up you need to have killed at least " +
													(weapon.getPowerChainRequiredKills() +
													powerUp.getRequiredKillsForSelfOnly()) + " monsters (enemies). "
															+ "You have killed " + strength + " enemies.");
			return false;
		}
	}
	
}
