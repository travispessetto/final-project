package com.pessetto.vitals;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.weapons.Weapon;

public abstract class Liveable {
	protected VitalPackage vitals;
	protected Weapon weapon;
	protected String name;
	
	public Liveable(String name)
	{
		vitals = new VitalPackage();
		vitals.life = 100;
		vitals.stamina = 100;
		this.name = name;
	}
	
	public  void setWeapon(Weapon weapon)
	{
		this.weapon = weapon;
	}
	
	public Weapon getWeapon()
	{
		return weapon;
	}
	
	public void takeVitals(VitalPackage vitalPackage)
	{
		vitals.life += vitalPackage.life;
		vitals.stamina += vitalPackage.stamina;
	}
	
	public VitalPackage getVitals()
	{
		return new VitalPackage(vitals);
	}

	public boolean isDead()
	{
		if(vitals.life <= 0 || vitals.stamina <= 0)
		{
			ConsoleWriter.getInstance().writeLine(name + " was killed.");
			return true;
		}
		else
		{
			return false;
		}
	}
}
