package com.pessetto.vitals;

//used more or less like a struct in C++ so everything is public
public class VitalPackage {
	public int life = 0;
	public int stamina = 0;
	
	public VitalPackage(){}
	public VitalPackage(VitalPackage copyable)
	{
		this.life = copyable.life;
		this.stamina = copyable.stamina;
	}
	
	public VitalPackage(int life, int stamina)
	{
		this.life = life;
		this.stamina = stamina;
	}
	
	public void addToSelf(VitalPackage pkg)
	{
		this.life += pkg.life;
		this.stamina += pkg.stamina;
	}
	
	public VitalPackage add(VitalPackage pkg)
	{
		return new VitalPackage(this.life + pkg.life, this.stamina + pkg.stamina);
	}
	
}
