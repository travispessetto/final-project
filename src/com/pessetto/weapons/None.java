package com.pessetto.weapons;

import com.pessetto.monsters.MonsterFactory;
import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.powerups.PowerUp;

public class None extends Weapon{
	
	public None()
	{
		damage = new VitalPackage(-1,-2);
	}

}
