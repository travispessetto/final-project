package com.pessetto.weapons;

import com.pessetto.monsters.MonsterFactory;
import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.powerups.PowerUp;

public class Sword extends Weapon{
	
	public Sword()
	{
		damage = new VitalPackage(-20,-10);
	}
	
	
}
