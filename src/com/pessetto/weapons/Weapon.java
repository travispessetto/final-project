package com.pessetto.weapons;

import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.powerups.PowerUp;

public abstract class Weapon {
	protected PowerUp powerUp;
	protected VitalPackage damage;
	
	public VitalPackage getDamage() 
	{
		if(powerUp != null)
		{
			return damage.add(powerUp.getDamage());
		}
		else
		{
			return damage;
		}
	}

	public void addPowerUp(PowerUp powerUp) {
		if(this.powerUp == null)
		{
			this.powerUp = powerUp;
		}
	}
	
	public int getPowerChainRequiredKills()
	{
		if(powerUp != null)
		{
			return powerUp.getPowerChainRequiredKills();
		}
		else
		{
			return 0;
		}
	}
}
