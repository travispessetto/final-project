package com.pessetto.weapons.powerups;

import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.Weapon;

public class Fire extends PowerUp{
	
	public Fire()
	{
		super(10,new VitalPackage(-10,-30));
	}
	@Override
	public VitalPackage getDamage() {
		return damage;
	}

}
