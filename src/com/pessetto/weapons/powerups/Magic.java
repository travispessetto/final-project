package com.pessetto.weapons.powerups;

import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.Weapon;

public class Magic extends PowerUp {
	
	public Magic()
	{
		super(20,new VitalPackage(-20,-20));
	}
	@Override
	public VitalPackage getDamage() {
		return damage;
	}

}
