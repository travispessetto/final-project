package com.pessetto.weapons.powerups;

import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.Weapon;

public class Poison extends PowerUp {
	
	public Poison()
	{
		super(100,new VitalPackage(0,-40));
	}
	
	@Override
	public VitalPackage getDamage() {
		return damage;
	}

}
