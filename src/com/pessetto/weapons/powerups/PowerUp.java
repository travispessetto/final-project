package com.pessetto.weapons.powerups;

import com.pessetto.io.ConsoleWriter;
import com.pessetto.monsters.MonsterFactory;
import com.pessetto.vitals.VitalPackage;
import com.pessetto.weapons.Weapon;

public abstract class PowerUp extends Weapon {
	protected int requiredKills;
	protected PowerUp powerUpNode;
	
	public PowerUp(int requiredKills, VitalPackage damage)
	{
		this.requiredKills = requiredKills;
		this.damage = damage;
		this.powerUpNode = null;
	}
	
	@Override
	public void addPowerUp(PowerUp powerUp)
	{
		PowerUp currentNode = this.getNextPowerUp();
		while(currentNode != null)
		{
			currentNode = currentNode.getNextPowerUp();
		}
		
		currentNode = powerUp;
	}
	
	@Override
	public VitalPackage getDamage()
	{
		return this.getPowerUpDamage();
	}
	
	private PowerUp getNextPowerUp()
	{
		return powerUpNode;
	}
	
	protected VitalPackage getPowerUpDamage()
	{
		VitalPackage damage = new VitalPackage(0,0);
		damage.addToSelf(this.damage);
		if(powerUpNode != null)
		{
			damage.addToSelf(powerUpNode.getPowerUpDamage());
		}
		return damage;
	}
	
	private PowerUp nextPowerUpNode()
	{
		return this.powerUpNode;
	}
	
	public int getPowerChainRequiredKills()
	{
		PowerUp nextPowerUp = nextPowerUpNode();
		int requiredKills = this.requiredKills;
		while(nextPowerUp != null)
		{
			requiredKills += nextPowerUp.getRequiredKillsForSelfOnly();
			nextPowerUp = nextPowerUpNode();
		}
		return requiredKills;
	}
	
	public int getRequiredKillsForSelfOnly()
	{
		return this.requiredKills;
	}
	
}
